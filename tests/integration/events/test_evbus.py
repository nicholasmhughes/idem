import asyncio
import json
import subprocess
import sys

from .conftest import KAFKA_PROFILE_NAME
from .conftest import RABBITMQ_PROFILE_NAME


async def test_exec(hub, event_loop):
    body = "message body"
    profile = "test_idem_ingress"
    ingress_profiles = {"internal": [{profile: {}}]}

    listener = event_loop.create_task(hub.evbus.init.start(ingress_profiles))
    await hub.evbus.init.join()

    # Put an event on the queue
    try:
        ret = await hub.exec.test.event(body=body, ingress_profile=profile)
        assert ret.result is True
        queue: asyncio.Queue = await hub.ingress.internal.get()
        msg = await queue.get()

        assert json.loads(msg) == {
            "tags": {},
            "message": "message body",
            "run_name": "cli",
        }
    finally:
        await hub.evbus.init.stop()
        await listener


async def test_kafka(hub, runpy, kafka):
    body = "message body"

    # Fire an event using the cli
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "exec",
            "test.event",
            f'body="{body}"',
            f"ingress_profile={KAFKA_PROFILE_NAME}",
            f"--acct-file={hub.OPT.acct.acct_file}",
            f"--acct-key={hub.OPT.acct.acct_key}",
        ]
    )
    assert proc.wait() == 0

    # Verify that it was received in kafka
    received_message = await kafka.getone()

    assert json.loads(received_message.value) == {
        "message": "message body",
        "tags": {},
        "run_name": "cli",
    }


async def test_rabbitmq(hub, runpy, rabbitmq):
    body = "message body"

    # Fire an event using the cli
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "exec",
            "test.event",
            f'body="{body}"',
            f"ingress_profile={RABBITMQ_PROFILE_NAME}",
            f"--acct-file={hub.OPT.acct.acct_file}",
            f"--acct-key={hub.OPT.acct.acct_key}",
        ]
    )
    assert proc.wait() == 0

    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == {
        "message": "message body",
        "tags": {},
        "run_name": "cli",
    }
