unprocessed_profile:
  acct.profile:
    - provider_name: test
    - key_1: value_1

unprocessed_result:
  test.acct:
    - acct_profile: unprocessed_profile
    - require:
        - acct: unprocessed_profile
