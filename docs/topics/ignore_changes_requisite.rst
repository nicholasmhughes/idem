===========================
ignore_changes Requisite
===========================

ignore_changes can be used in sls blocks to prevented parameters to be updated on brown-field resources.
If a parameter is specified under the ignore_changes and this parameter exists in the ESM, Idem will use the ESM value over the sls value for this parameter.

In the following example, State_A is a existing resource that is not managed by Idem. State_B refers State_A's tags during creation.
However, since State_A is not managed by Idem, its tags may change later and it will lead to an update of State_B's tags.
ignore_changes requisite prevents updating State_B's tags as State_A changes.

.. code-block:: sls

   State_A:
      cloud.instance.search:
        - name: unmanaged-instance
        - resource_id: i-12345678

   State_B:
      cloud.instance.present:
        - name: my-resource
        - tags: ${cloud.instance:State_A:tags}
        - ignore_changes:
          - tags

Note: ignore_changes requisite only works when the enforced State_B exists in ESM cache.
